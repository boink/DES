ccc = cc -Wall -Wextra -Werror -g
main: main.o des.o
	$(ccc) -o main main.o des.o
main.o: des.o main.c des.h
	$(ccc) -c main.c
des.o: des.c
	$(ccc) -c des.c
.PHONY: clean
clean:
	rm des.o main.o main
