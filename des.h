/* Copyright (C) 2021 by boink */
/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <https://www.gnu.org/licenses/>. */
#include <stdint.h>

uint64_t des_encrypt(uint64_t plaintext, uint64_t full_key,int do_encryption);
int des_encrypt_file(FILE *in, FILE *out, uint64_t key, int enc_or_dec);
uint64_t generate_random_key();


